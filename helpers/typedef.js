/**
 * CountryCompact
 * @typedef {Object} CountryCompact
 * @property {string} type
 * @property {integer} id
 * @property {string} uid
 * @property {string} name
 */

/**
 * CustomerStage
 * @typedef {Object} CustomerStage
 * @property {string} type
 * @property {integer} id
 * @property {string} uid
 * @property {integer} priority
 * @property {bool} defaultStage
 * @property {Stage} stage
 * @property {bool} acitve
 */

/**
 * Gender
 * @enum {string}
 */
var Gender = Object.freeze({men: "men", women: "women"});

/**
 * LiveFeed
 * @typedef {Object} LiveFeed
 * @property {string} type
 * @property {integer} id
 * @property {string} uid
 * @property {integer|null} customerId
 * @property {integer} matchId
 * @property {bool} prioritized
 * @property {User} user
 */

/**
 * Match
 * @typedef {Object} Match
 * @property {string} type
 * @property {integer} id
 * @property {string} uid
 * @property {string} name
 * @property {Date} timestamp
 * @property {integer} matchStatusId
 * @property {integer|null} matchTypeId
 * @property {integer|null} round
 * @property {integer|null} attendance
 * @property {Result} result
 * @property {StadiumCompact|null} stadium
 * @property {TeamInMatch} homeTeam
 * @property {TeamInMatch} awayTeam
 * @property {StageWithCompactTournament} stage
 * @property {Referee[]} referees
 * @property {MatchEvent[]} matchEvents
 * @property {LiveFeed[]|null} liveFeeds
 */

/**
 * MatchCompact
 * @typedef {Object} MatchCompact
 * @property {string} type
 * @property {integer} id
 * @property {string} uid
 * @property {string} name
 * @property {Date} timestamp
 * @property {integer.<1,2,3,4,5,8,9,10,11,12,13,14,15,16,17>} matchStatusId
 * @property {integer.<1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21>|null} matchTypeId
 * @property {integer|null} round
 * @property {integer|null} attendance
 * @property {Result} result
 * @property {StadiumCompact|null} stadium
 * @property {TeamCompact} homeTeam
 * @property {TeamCompact} awayTeam
 * @property {StageWithCompactTournament} stage
 */

/**
 * MatchEvent
 * @typedef {Object} MatchEvent
 * @property {string} type
 * @property {integer} id
 * @property {string} uid
 * @property {string|null} comment
 * @property {integer.<1,2,3,4,5,8,9,10,11,12,13,14,15,16,17,18,19,20,23,24,25,26,27,28,29,30,31,32,43>} matchEventTypeId
 * @property {integer.<1,2,3,4,5,6,7,8,9,10,11>} periodId
 * @property {bool} important
 * @property {integer} sorting
 * @property {integer|null} time
 * @property {integer|null} overtime
 * @property {Team|null} team
 * @property {Person|null} person
 * @property {integer} matchId
 * @property {liveFeedId|null} liveFeedId
 */

/**
 * MatchEventTypeId
 * @enum {integer}
 */
var MatchEventTypeId = Object.freeze({COMMENT: 1,
	GOAL: 2,
	RED_CARD: 3,
	YELLOW_CARD: 4,
	ASSIST: 5,
	OWN_GOAL: 8,
	PENALTY: 9,
	PENALTY_GOAL: 10,
	PENALTY_MISS: 11,
	SECOND_YELLOW_CARD: 12,
	DISALLOWED_GOAL: 13,
	MATCH_KICKS_OFF: 14,
	HALF_TIME: 15,
	MATCH_ENDS: 16,
	END_SECOND_HALF_MATCH_CONTINUES: 17,
	HALF_TIME_EXTRA_TIME: 18,
	PENALTIES_SHOOTOUT: 19,
	END_SECOND_HALF_EXTRA_TIME: 20,
	PLAYER_SUBSTITUTED: 23,
	PLAYER_ON: 24,
	CORNER: 25,
	START_SECOND_HALF: 26,
	CORNER_GOAL: 27,
	START_FIRST_HALF_EXTRA_TIME: 28,
	START_SECOND_HALF_EXTRA_TIME: 29,
	PENALTIES_SHOOTOUT_GOAL: 30,
	PENALTIES_SHOOTOUT_MISS: 31,
	PENALTIES_SHOOTOUT_PENALTY: 32,
	LINEUP_READY: 43});

/**
 * MatchStatusId
 * @enum {integer}
 */
var MatchStatusId = Object.freeze({PLAYED: 1,
	NOT_PLAYED: 2,
	POSTPONED: 3,
	ABANDONED: 4,
	WILL_NOT_BE_PLAYED: 5,
	FIRST_HALF: 8,
	HALF_TIME: 9,
	SECOND_HALF: 10,
	FIRST_HALF_EXTRA_TIME: 11,
	SECOND_HALF_EXTRA_TIME: 12,
	PENALTY_SHOOTOUT: 13,
	PAUSE_BEFORE_EXTRA_TIME: 14,
	HALF_TIME_EXTRA_TIME: 15,
	PLAYED_BUT_CANCELLED: 16,
	PLAYED_BUT_NOT_COUNTING: 17})

/**
 * MatchTypeId
 * @enum {integer}
 */
var MatchTypeId = Object.freeze({FINAL: 1,
	BRONZE_FINAL: 2,
	SEMI_FINAL: 3,
	QUARTER_FINAL: 4,
	ROUND_OF_16: 5,
	FIFTH_PLACE_PLAY_OFF: 6,
	MINOR_SEMI_FINAL: 7,
	MAJOR_SEMI_FINAL: 8,
	PRELIMINARY_FINAL: 9,
	ELIMINATION_FINAL: 10,
	SEVENTH_PLACE_PLAY_OFF: 11,
	NINTH_PLACE_PLAY_OFF: 12,
	ELEVENTH_PLACE_PLAY_OFF: 13,
	ELIMINATION_SEMI_FINAL: 14,
	PRELIMINARY_SEMI_FINAL: 15,
	QUALIFYING_FINAL: 16,
	PLAY_OFF_MATCH: 17,
	THIRTEENTH_PLACE_PLAY_OFF: 18,
	FIFTEENTH_PLACE_PLAY_OFF: 19,
	SEVENTEENTH_PLACE_PLAY_OFF: 20,
	NINETEENTH_PLACE_PLAY_OFF: 21})

/**
 * PeriodId
 * @enum {integer}
 */
var PeriodId = Object.freeze({BEFORE_MATCH: 1,
	FIRST_HALF: 2,
	HALF_TIME: 3,
	SECOND_HALF: 4,
	PAUSE_BEFORE_EXTRA_TIME: 5,
	FIRST_HALF_EXTRA_TIME: 6,
	HALF_TIME_EXTRA_TIME: 7,
	SECOND_HALF_EXTRA_TIME: 8,
	PAUSE_BEFORE_PENALTIES: 9,
	PENALTIES_SHOOTOUT: 10,
	AFTER_MATCH: 11});

/**
 * Person
 * @typedef {Object} Person
 * @property {string} type
 * @property {integer} id
 * @property {string} uid
 * @property {string} name
 */

/**
 * PersonInMatch
 * @typedef {Object} PersonInMatch
 * @property {string} name
 * @property {integer} id
 * @property {Team} team
 * @property {Match} match
 * @property {bool} startsMatch
 * @property {integer|null} entersFieldMinute
 * @property {integer|null} leavesFieldMinute
 *
 */

/**
 * PlayerTransfer
 * @typedef {Object} PlayerTransfer
 * @property {string} date
 * @property {Person} person
 * @property {integer} id
 * @property {Team} fromTeam
 * @property {Team} toTeam
 * @property {bool} isALoan
 * @property {string} toDate
 */

/**
 * Referee
 * @typedef {Object} Referee
 * @property {string} type
 * @property {string} uid
 * @property {integer} id
 * @property {string} name
 * @property {Integer.<1,2,4,5>} refereeTypeId
 */

/**
 * RefereeTypeId
 * @enum {integer}
 */
var RefereeTypeId = Object.freeze({REFEREE: 1,
	ASSISTANT_REFEREE: 2,
	FOURTH_OFFICIAL: 4,
	ADDITIONAL_ASSISTANT_REFEREE: 5});

/**
 * Result
 * @typedef {Object} Result
 * @property {string} type
 * @property {string} uid
 * @property {integer} homeScore90
 * @property {integer} awayScore90
 * @property {integer} homeScore45
 * @property {integer} awayScore45
 * @property {integer} homeScore105
 * @property {integer} awayScore105
 * @property {integer} homeScore120
 * @property {integer} awayScore120
 * @property {integer} homeScorePenalties
 * @property {integer} awayScorePenalties
 */

/**
 * StadiumCompact
 * @typedef {Object} StadiumCompact
 * @property {string} type
 * @property {integer} id
 * @property {string} uid
 * @property {string} name
 */

/**
 * Stage
 * @typedef {Object} Stage
 * @property {String} type
 * @property {Integer} id
 * @property {String} uid
 * @property {String} name
 * @property {String} fullName
 * @property {Integer} yearStart
 * @property {Integer} yearEnd
 * @property {String} groupName
 * @property {Date} dateStart
 * @property {Date} dateEnd
 * @property {Tournament} tournament
 * @property {Integer.<1,2,3>} stageTypeId
 * @property {Integer.<1,2,5>} visibilityId
 * @property {Array.<{ratings:boolean,
    assists:boolean,
    attendances:boolean,
    corners:boolean,
    goalscorers:boolean,
    halfTimeScore:boolean,
    minutesPlayed:boolean,
    penalties:boolean,
    redCards:boolean,
    referees:boolean,
    shots:boolean,
    yellowCards:boolean,
	indirectAssists: boolean}>} data
 * @property {String} comment
 */

/**
 * StageWithCompactTournament
 * @typedef {Object} StageWithCompactTournament
 * @property {String} type
 * @property {Integer} id
 * @property {String} uid
 * @property {String} name
 * @property {String} fullName
 * @property {Integer} yearStart
 * @property {Integer} yearEnd
 * @property {String} groupName
 * @property {Date} dateStart
 * @property {Date} dateEnd
 * @property {TournamentCompact} tournament
 * @property {Integer.<1,2,3>} stageTypeId
 * @property {Integer.<1,2,5>} visibilityId
 * @property {Array.<{ratings:boolean,
    assists:boolean,
    attendances:boolean,
    corners:boolean,
    goalscorers:boolean,
    halfTimeScore:boolean,
    minutesPlayed:boolean,
    penalties:boolean,
    redCards:boolean,
    referees:boolean,
    shots:boolean,
    yellowCards:boolean,
	indirectAssists: boolean}>} data
 * @property {String} comment
 */

/**
 * StageTypeId
 * @enum {integer}
 */
var StageTypeId = Object.freeze({LEAGUE: 1,
	CUP: 2,
	OPEN: 3})

/**
 * Team
 * @typedef {Object} Team
 * @property {string} type
 * @property {integer} id
 * @property {string} uid
 * @property {string} name
 */

/**
 * TeamCompact
 * @property {string} type
 * @property {integer} id
 * @property {string} uid
 * @property {string} name
 */

/**
 * Tournament
 * @typedef {Object} Tournament
 * @property {string} type
 * @property {integer} id
 * @property {string} uid
 * @property {string} name
 * @property {string.<men,women>} gender
 * @property {integer} level
 * @property {CountryCompact} countryCompact
 * @property {bool} neutralVenues
 * @property {integer.<1,2,3,4,5,6,7>} tournamentTypeId
 * @property {integer.<1,2,5>} visibilityId
 */

/**
 * TournamentTypeId
 * @enum {integer}
 */
var TournamentTypeId = Object.freeze({NATIONAL_TEAMS : 1,
	CLUB_TOURNAMENTS: 2,
	INTERNATIONAL_CLUB_TOURNAMENTS: 3,
	CLUB_FRIENDLIES: 4,
	NATIONAL_TEAM_FRIENDLIES: 5,
	OPEN: 6,
	YOUTH: 7});

/**
 * TournamentCompact
 * @typedef {Object} TournamentCompact
 * @property {string} type
 * @property {integer} id
 * @property {string} uid
 * @property {string} name
 */

/**
 * User
 * @typedef {Object} User
 * @property {string} type
 * @property {integer} id
 * @property {string} uid
 * @property {string} name
 * @property {string|null} twitterHandle
 */

/**
 * VisiblityId
 * @enum {integer}
 */
var VisibilityId = Object.freeze({SHOW : 1,
	SHOW_ONLY_ON_NIFS_NO: 2,
	DO_NOT_SHOW: 5});