/**
 * Created by kurt on 29/09/15.
 */

var express = require('express');
var app = express();
var http = require('http').Server(app);

app.get('/', function(req, res){
	res.sendFile(__dirname + '/index.html');
});

app.use('/models', express.static(__dirname + '/models'));
app.use('/helpers', express.static(__dirname + '/helpers'));
app.use('/config', express.static(__dirname + '/config'));

http.listen(3000, function(){
	console.log('listening on *:3000');
});