/**
 * Created by kurt on 05/01/16.
 */

function ImportantEventsModel () {

};

ImportantEventsModel.prototype.loadImportantEvents = function() {

	$.ajax({
		headers: {
			Accept : "application/json",
		},
		url: apiURL + "matchEvents/?latest=1",
		timeout: 10000,
		success: function(data) {
			importantEventsModel.printImportantEvents(data);
		},
		error: function(jqXHR, textStatus, errorThrown) {
			console.info(textStatus);
			console.info(errorThrown);
		}
	});
};

/**
 *
 * @param {MatchEvent[]} events
 */
ImportantEventsModel.prototype.printImportantEvents = function(events) {

	for (var i in events) {
		importantEventsModel.addEvent(events[i]);
	}
};

ImportantEventsModel.prototype.addEvent = function(event) {

	var str = matchModel.getEventString(event, importantEvents);
	if (str) {
		importantEvents.push(event);
		if ($("#importantEvent_" + event.id).html()) {
			$("#importantEvent_" + event.id).html(str);
		}
		else {
			$('#importantEvents').append("<div class='importantEvent' id='importantEvent_" + event.id + "' matchId='" + event.matchId + "' sorting='" + event.sorting + "'><b>" + event.match.name + "</b><br>" + str + "</div>");
		}
	}
	else {
		var otherMatchEvent = false;
		for (var j in importantEvents) {
			if (event.sorting == importantEvents[j].sorting && event.id != importantEvents[j].id && event.matchId == importantEvents[j].matchId) {
				otherMatchEvent = importantEvents[j];
				break;
			}
		}
		if (otherMatchEvent) {
			importantEvents.push(event);
			var str2 = "";
			if (otherMatchEvent.matchEventTypeId == MatchEventTypeId.GOAL) {
				if (event.matchEventTypeId == MatchEventTypeId.ASSIST) {
					str2 = " Assist: " + event.person.name;
				}
				else {
					str2 = " Indirekte assist: " + event.person.name;
				}
			}
			else if (otherMatchEvent.matchEventTypeId == MatchEventTypeId.PLAYER_SUBSTITUTED) {
				str += " Inn: " + event.person.name;
			}
			else if (otherMatchEvent.matchEventTypeId == MatchEventTypeId.OWN_GOAL) {
				str += " Indirekte assist: " + event.person.name;
			}
			var text = $("#importantEvent_" + otherMatchEvent.id).html();
			text += str2;
			$("#importantEvent_" + otherMatchEvent.id).html(text);
		}
	}
}