/**
 * Created by kurt on 01/10/15.
 */

function CustomerStagesModel () {

};

/**
 *
 * @param {Integer} customerId
 * @param {Date} date
 */
CustomerStagesModel.prototype.loadCustomerStages = function(customerId) {

	$.ajax({
		headers: {
			Accept : "application/json",
		},
		url: apiURL + "customers/" + customerId + "/stages/",
		timeout: 10000,
		success: function(data) {
			customerStages = data;
			if (allMatchesData) {
				matchListModel.printMatches(allMatchesData, customerStages);
			}
		},
		error: function(jqXHR, textStatus, errorThrown) {
			console.info(textStatus);
			console.info(errorThrown);
		}
	});
}
