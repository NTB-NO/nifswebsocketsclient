/**
 * Created by kurt on 01/10/15.
 */

Date.prototype.yyyymmdd = function() {
	var yyyy = this.getFullYear().toString();
	var mm = (this.getMonth() + 1).toString();
	var dd  = this.getDate().toString();
	return yyyy + "-" + (mm[1] ? mm : "0" + mm[0]) + "-" + (dd[1] ? dd : "0" + dd[0]);
};

function MatchListModel () {

};

/**
 *
 * @param {Integer} customerId
 * @param {Date} date
 */
MatchListModel.prototype.loadMatches = function(customerId, date) {

	$.ajax({
		headers: {
			Accept : "application/json",
		},
		url: apiURL + "matches/?date=" + date.yyyymmdd() + "&inCustomerStages=1",
		timeout: 10000,
		success: function(data) {

			allMatchesData = data;
			if (customerStages) {
				matchListModel.printMatches(data, customerStages);
			}
		},
		error: function(jqXHR, textStatus, errorThrown) {
			console.info(textStatus);
			console.info(errorThrown);
		}
	});
}

/**
 *
 * @param {MatchCompact[]} matches
 * @param {CustomerStage[]} customerStages
 */
MatchListModel.prototype.printMatches = function(matches, customerStages) {

	var temp = Array();
	for (var i in customerStages) {
		for (var j in matches) {
			if (customerStages[i].stage.id == matches[j].stage.id) {
				temp.push(matches[j]);
			}
		}
	}

	var stageId = -1;
	var container = "";
	for (var i in temp) {
		if (stageId != temp[i].stage.id) {
			if (container.length) {
				$("#matchList").append("<div class='stageContainer' id='stageContainer_" + stageId  + "'>" + container + "</div>");
				container = "";
			}
			$("#matchList").append("<div class='stageHeader' id='stageHeader_" + temp[i].stage.id  + "'><div>" + temp[i].stage.fullName + "</div></div>")
		}

		container += this.getMatchHTML(temp[i]);

		stageId = temp[i].stage.id;
	}
	if (container.length) {
		$("#matchList").append("<div class='stageContainer' id='stageContainer_" + temp[i].stage.id  + "'>" + container + "</div>");
	}
};

/**
 *
 * @param {Match} match
 */
MatchListModel.prototype.getMatchHTML = function(match) {

	matches.push(match);

	var time = matchModel.getTime(match);
	var score = match.result.homeScore90 + " - " + match.result.awayScore90;
	var result = match.matchStatusId == MatchStatusId.NOT_PLAYED ? time : score;

	var liveFeed = false;
	if (match.liveFeeds) {
		for (var i in match.liveFeeds) {
			if (match.liveFeeds[i].prioritized) {
				liveFeed = match.liveFeeds[i];
				break;
			}
		}
	}

	return "<div class='match' id='match_" + match.id + "'><div id='liveFeed_" + match.id + "' style='float: left;padding:5px 2px;'>" + (liveFeed ? "L" : "&nbsp;") + "</div><div class='title'>" + match.name + "</div><div class='result" + (match.matchStatusId == MatchStatusId.PLAYED ? " resultPlayed" : "") + "' id='matchList_result_" + match.id + "'>" + result + "</div><div class='clear'>&nbsp;</div></div>";
};

/**
 *
 * @param {Integer} stageId
 */
MatchListModel.prototype.loadMatchesForStage = function(stageId) {

	$.ajax({
		headers: {
			Accept : "application/json"
		},
		url: apiURL + "stages/" + stageId + "/matches/?date=" + date.yyyymmdd(),
		timeout: 10000,
		success: function(data) {
			matchListModel.updateMatchListForStage(data, stageId);
		},
		error: function(jqXHR, textStatus, errorThrown) {
			console.info(textStatus);
			console.info(errorThrown);
		}
	});
};

/**
 *
 * @param {MatchCompact[]} matches
 * @param {Integer} stageId
 */
MatchListModel.prototype.updateMatchListForStage = function(matches, stageId) {

	$("#stageContainer_" + stageId).html("");

	var container = "";
	for (var i in matches) {
		container += matchListModel.getMatchHTML(matches[i]);
	}

	$("#stageContainer_" + stageId).html(container);
};