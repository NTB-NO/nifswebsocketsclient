/**
 * Created by kurt on 07/10/15.
 */


function MatchModel () {

};

MatchModel.prototype.loadMatch = function(matchId) {

	$.ajax({
		headers: {
			Accept : "application/json",
		},
		url: apiURL + "matches/" + matchId,
		timeout: 10000,
		success: function(data) {
			matchModel.match = data;
			if (!matchModel.match.matchEvents) {
				matchModel.match.matchEvents = Array();
			}

			matchModel.printMatch(data);
			matchModel.printLineup(data);
			matchModel.printEvents(data);
		},
		error: function(jqXHR, textStatus, errorThrown) {
			console.info(textStatus);
			console.info(errorThrown);
		}
	});
}

/**
 *
 * @param {Match} match
 */
MatchModel.prototype.printLineup = function(match) {
	$("#homeTeamNameLineup").html(match.homeTeam.name);
	$("#awayTeamNameLineup").html(match.awayTeam.name);

	//if (match.isLineupConfirmed) {
		for (var i in match.homeTeam.persons) {
			if (match.homeTeam.persons[i].startsMatch) {
				var str = "<div id='homeLineup_" + match.homeTeam.persons[i].id + "'>" + (match.homeTeam.persons[i].shirtNumber ? match.homeTeam.persons[i].shirtNumber : "") + " " + match.homeTeam.persons[i].name;
				if (match.homeTeam.persons[i].position) {
					str += " (" + match.homeTeam.persons[i].position.x + ", " + match.homeTeam.persons[i].position.y + ")</div>";
				}
				else {
					str += "</div>";
				}
				$('#homeLineup').append(str);
			}
			else {
				$('#homeBench').append("<div id='homeLineup_" + match.homeTeam.persons[i].id + "'>" + (match.homeTeam.persons[i].shirtNumber ? match.homeTeam.persons[i].shirtNumber : "") + " " + match.homeTeam.persons[i].name + "</div>");
			}
		}

		for (var i in match.awayTeam.persons) {
			if (match.awayTeam.persons[i].startsMatch) {
				var str = "<div id='awayLineup_" + match.awayTeam.persons[i].id + "'>" + (match.awayTeam.persons[i].shirtNumber ? match.awayTeam.persons[i].shirtNumber : "") + " " + match.awayTeam.persons[i].name;
				if (match.awayTeam.persons[i].position) {
					str += " (" + match.awayTeam.persons[i].position.x + ", " + match.awayTeam.persons[i].position.y + ")</div>";
				}
				else {
					str += "</div>";
				}
				$('#awayLineup').append(str);
			}
			else {
				$('#awayBench').append("<div id='awayLineup_" + match.awayTeam.persons[i].id + "'>" + (match.awayTeam.persons[i].shirtNumber ? match.awayTeam.persons[i].shirtNumber : "") + " " + match.awayTeam.persons[i].name + "</div>");
			}
		}
	//}
}

/**
 *
 * @param {Match} match
 */
MatchModel.prototype.printEvents = function(match) {

	//console.log(match.matchEvents);
	if (match.matchEvents) {
		for (var i in match.matchEvents) {

			this.addMatchEvent(match.matchEvents[i], match.matchEvents);
		}
	}
}

/**
 *
 * @param {Match} match
 */
MatchModel.prototype.printMatch = function(match) {

	$('#matchName').html(match.name);
	if (match.matchStatusId != MatchStatusId.NOT_PLAYED) {
		var score = match.result.homeScore90 + " - " + match.result.awayScore90;
		$('#matchResult').html(score);
	}
	else {
		$('#matchResult').html("");
	}

	var date = new Date(match.timestamp);
	var hours = date.getHours().toString().length == 1 ? "0" +  date.getHours() : date.getHours();
	var minutes = date.getMinutes().toString().length == 1 ? "0" + date.getMinutes() : date.getMinutes();
	var yyyy = date.getFullYear().toString();
	var mm = (date.getMonth() + 1).toString();
	var dd  = date.getDate().toString();
	var month = (mm[1] ? mm : "0" + mm[0]);
	var day = (dd[1] ? dd : "0" + dd[0]);

	$('#matchDate').html(day + "." + month + " " + yyyy + " kl " + hours + ":" + minutes);
	if (match.stadium) {
		$('#matchStadium').html(match.stadium.name);
	}
	if (match.stage) {
		$('#matchTournament').html(match.stage.fullName);
		if (match.liveFeeds) {
			var liveFeed = null;
			for (var i in match.liveFeeds) {
				if (match.liveFeeds[i].prioritized) {
					liveFeed = match.liveFeeds[i];
				}
				if (match.liveFeeds[i].customerId == customerId) {
					liveFeed = match.liveFeeds[i];
					break;
				}
			}

			$('#matchReporter').html(liveFeed.user.name);
		}
	}
	if (match.referees) {
		var refereeString = "";
		for (var i in match.referees) {
			refereeString += "<span id='referee_" + match.referees[i].id + "'>" + match.referees[i].name;
			if (match.referees[i].refereeTypeId == RefereeTypeId.REFEREE) {
				refereeString += " (hoveddommer), "
			}
			else if (match.referees[i].refereeTypeId == RefereeTypeId.ASSISTANT_REFEREE) {
				refereeString += " (assistentdommer), "
			}
			else if (match.referees[i].refereeTypeId == RefereeTypeId.FOURTH_OFFICIAL) {
				refereeString += " (fjerdedommer), "
			}
			else if (match.referees[i].refereeTypeId == RefereeTypeId.ADDITIONAL_ASSISTANT_REFEREE) {
				refereeString += " (ekstra linjedommer), "
			}
			refereeString += "</span>"
		}
		$('#matchReferees').html(refereeString);
	}
}

/**
 *
 * @param {MatchEvent} matchEvent
 * @param {MatchEvent[]} matchEvents
 * @param {string} elementId
 * @returns {boolean}
 */
MatchModel.prototype.addMatchEvent = function(matchEvent, matchEvents, elementId) {

	var str = this.getEventString(matchEvent, matchEvents);
	if (!str) {
		return false;
	}

	if (elementId) {
		$(elementId).html(str);
	}
	else {
		$('#matchEvents').append("<li id='matchEvent_" + matchEvent.id + "'>" + str + "</li>");
	}

	return true;
};

/**
 *
 * @param {MatchEvent} matchEvent
 * @param {MatchEvent[]} matchEvents
 * @returns {bool|string}
 */
MatchModel.prototype.getEventString = function (matchEvent, matchEvents) {

	var typeString = null;
	var statusEvent = false;
	var goalEvent = false;
	var person2 = false;
	var indirect = false;
	if (matchEvent.matchEventTypeId == MatchEventTypeId.LINEUP_READY) {
		typeString = "Lagoppstilling klar";
		statusEvent = true;
	}
	else if (matchEvent.matchEventTypeId == MatchEventTypeId.MATCH_KICKS_OFF) {
		typeString = "Kampen starter";
		statusEvent = true;
	}
	else if (matchEvent.matchEventTypeId == MatchEventTypeId.GOAL) {
		typeString = "Mål!";
		goalEvent = true;
		for (var j in matchEvents) {
			if (matchEvents[j].sorting == matchEvent.sorting && matchEvent.id != matchEvents[j].id && matchEvents[j].matchId == matchEvent.matchId) {
				person2 = matchEvents[j].person;
				break;
			}
		}
	}
	else if (matchEvent.matchEventTypeId == MatchEventTypeId.OWN_GOAL) {
		typeString = "Selvmål!";
		goalEvent = true;
		for (var j in matchEvents) {
			if (matchEvents[j].sorting == matchEvent.sorting && matchEvent.id != matchEvents[j].id && matchEvents[j].matchId == matchEvent.matchId) {
				person2 = matchEvents[j].person;
				break;
			}
		}
	}
	else if (matchEvent.matchEventTypeId == MatchEventTypeId.PLAYER_SUBSTITUTED) {
		typeString = "Spiller ut";
		for (var j in matchEvents) {
			if (matchEvents[j].sorting == matchEvent.sorting && matchEvent.id != matchEvents[j].id && matchEvents[j].matchId == matchEvent.matchId) {
				person2 = matchEvents[j].person;
				indirect = matchEvents[j].matchEventTypeId == MatchEventTypeId.ASSIST ? false : true;
				break;
			}
		}
	}
	else if (matchEvent.matchEventTypeId == MatchEventTypeId.YELLOW_CARD) {
		typeString = "Gult kort";
	}
	else if (matchEvent.matchEventTypeId == MatchEventTypeId.RED_CARD) {
		typeString = "Rødt kort";
	}
	else if (matchEvent.matchEventTypeId == MatchEventTypeId.SECOND_YELLOW_CARD) {
		typeString = "Rødt kort (to gule)";
	}
	else if (matchEvent.matchEventTypeId == MatchEventTypeId.CORNER) {
		typeString = "Corner";
	}
	else if (matchEvent.matchEventTypeId == MatchEventTypeId.HALF_TIME) {
		typeString = "Pause";
		statusEvent = true;
	}
	else if (matchEvent.matchEventTypeId == MatchEventTypeId.START_SECOND_HALF) {
		typeString = "Start 2. omgang";
		statusEvent = true;
	}
	else if (matchEvent.matchEventTypeId == MatchEventTypeId.MATCH_ENDS) {
		typeString = "Slutt";
		statusEvent = true;
	}
	else {
		return false;
	}

	var str = "";

	if (statusEvent || goalEvent) {
		str += matchEvent.score + " ";
	}

	if (statusEvent) {
		str += typeString;
	}
	else {
		str += matchEvent.time + " " + typeString;
		if (matchEvent.person) {
			str += " " + matchEvent.person.name + " (" + matchEvent.team.name + ")";
		}
		else if (matchEvent.team) {
			str += " (" + matchEvent.team.name + ")";
		}
	}

	if (person2 && matchEvent.matchEventTypeId == MatchEventTypeId.PLAYER_SUBSTITUTED) {
		str += " Inn: " + person2.name;
	}
	if (person2 && matchEvent.matchEventTypeId == MatchEventTypeId.GOAL) {
		if (indirect) {
			str += " Indirekte assist: " + person2.name;
		}
		else {
			str += " Assist: " + person2.name;
		}
	}

	if (person2 && matchEvent.matchEventTypeId == MatchEventTypeId.OWN_GOAL) {
		str += " Indirekte assist: " + person2.name;
	}

	return str;
};

/**
 *
 * @param {MatchCompact} match
 * @returns {string}
 */
MatchModel.prototype.getTime = function(match) {

	var date = new Date(match.timestamp);
	var hours = date.getHours().toString().length == 1 ? "0" +  date.getHours() : date.getHours();
	var minutes = date.getMinutes().toString().length == 1 ? "0" + date.getMinutes() : date.getMinutes();
	return hours + ":" + minutes;
}